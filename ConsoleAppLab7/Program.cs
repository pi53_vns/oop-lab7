﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibraryLab7;

namespace ConsoleAppLab7
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Person person1 = new Person();
            person1.ShowInfo();
            Person person2 = new Person(5, 12, 1990);
            person2.ShowInfo();
            Person person3 = new Person("Іван", "Іванов");
            person3.ShowInfo();
            Person person4 = new Person(person2);
            person4.ShowInfo();

            Entrant entrant1 = new Entrant();
            entrant1.ShowInfo();
            Entrant entrant2 = new Entrant("Іван", "Іванов", 8, 11, 2000, 187, 184, 168, 179);
            entrant2.ShowInfo();
            Entrant entrant3 = new Entrant("Сергій", "Білий", 17, 10, 2000, 181, "Житомирський міський колегіум");
            entrant3.ShowInfo();
            Entrant entrant4 = new Entrant(entrant3);
            entrant4.ShowInfo();

            Student student1 = new Student();
            student1.ShowInfo();
            Student student2 = new Student("Петро", "Сидоров", 15, 7, 1996, 4, "ПІ-47", "ФІКТ");
            student2.ShowInfo();
            Student student3 = new Student(student2);
            student3.ShowInfo();
            Student student4 = new Student("Василь", "Петров", 28, 10, 1997, "ЖДУ");
            student4.ShowInfo();

            Teacher teacher1 = new Teacher();
            teacher1.ShowInfo();
            Teacher teacher2 = new Teacher("Андрій", "Морозов", 8, 6, 1985, "Декан факультету інформаційно-комп'ютерних технологій");
            teacher2.ShowInfo();
            Teacher teacher3 = new Teacher("Анатолій", "Голубєв", 4, 9, 1975, "Автоматизації", "КПІ");
            teacher3.ShowInfo();
            Teacher teacher4 = new Teacher(teacher2);
            teacher4.ShowInfo();           

            LibraryUser libuser1 = new LibraryUser();
            libuser1.ShowInfo();
            LibraryUser libuser2 = new LibraryUser("Петро", "Васильєв", 22, 3, 1992, 2015, 35);
            libuser2.ShowInfo();
            LibraryUser libuser3 = new LibraryUser("Іван", "Кошкін", 15, 10, 1984, 458405);
            libuser3.ShowInfo();
            LibraryUser libuser4 = new LibraryUser(libuser2);
            libuser4.ShowInfo();
        }
    }
}
