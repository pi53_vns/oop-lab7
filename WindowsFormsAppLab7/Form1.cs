﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using LibraryShapes;

namespace WindowsFormsAppLab7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonDraw_Click(object sender, EventArgs e)
        {
            Graphics gr = pictureBoxShapes.CreateGraphics();
            Random rnd = new Random();
            ShapePoint[] shapes = new ShapePoint[20];
            int menu;
            for (int i = 0; i < shapes.Length; i++)
            {
                menu = rnd.Next(5);
                if (menu == 0)
                    shapes[i] = new ShapePoint();
                if (menu == 1)
                    shapes[i] = new Line();
                if (menu == 2)
                    shapes[i] = new LibraryShapes.Rectangle();
                if (menu == 3)
                    shapes[i] = new Circle();
                if (menu == 4)
                    shapes[i] = new Ellipse();

                shapes[i].Draw(gr);
                Thread.Sleep(500);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Graphics gr = pictureBoxShapes.CreateGraphics();
            gr.Clear(Color.White);
        }
    }
}