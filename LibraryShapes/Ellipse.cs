﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LibraryShapes
{
    public class Ellipse : Circle
    {
        protected int RadiusEll;

        Random rnd = new Random();
        //Конструктор по замовчуванню
        public Ellipse() : base()
        {
            RadiusEll = rnd.Next(51);
        }
        //Конструктор з параметрами
        public Ellipse(int x, int y, int radius, int radiusEll, Color color) : base(x, y, radius, color)
        {
            RadiusEll = radiusEll;
        }
        //Конструктор копіювання
        public Ellipse(Ellipse ell) : base(ell)
        {
            RadiusEll = ell.RadiusEll;
        }

        public void SetEllipseRadius(int radiusEll)
        {
            RadiusEll = radiusEll;
        }

        public override void Draw(Graphics gr)
        {            
            Pen p = new Pen(Color, 3);
            gr.DrawEllipse(p, X, Y, Radius, RadiusEll);
        }

    }
}