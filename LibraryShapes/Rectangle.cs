﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LibraryShapes
{
    public class Rectangle : Line
    {
        Random rnd = new Random();

        //Конструктор по замовчуванню
        public Rectangle()
        {
            X = rnd.Next(30,100);
            Y = rnd.Next(30,163);
            Color = Color.FromArgb(rnd.Next(20, 256), rnd.Next(20, 256), rnd.Next(20, 256));
            X1 = rnd.Next(30,351);
            Y1 = rnd.Next(30,301);
        }

        //Конструктор з параметрами
        public Rectangle(int x, int y, Color color, int x1, int y1) : base(x, y, color, x1, y1)
        {

        }

        //Конструктор копіювання
        public Rectangle(Rectangle rectangle) : base(rectangle)
        {
            X1 = rectangle.X1;
            Y1 = rectangle.Y1;
        }

        public void SetRectangle(int x, int y, int x1, int y1)
        {
            X = x;
            Y = y;
            X1 = x1;
            Y1 = y1;
        }

        public override void Draw(Graphics gr)
        {            
            Pen p = new Pen(Color, 3);
            gr.DrawRectangle(p, X, Y, (X1 - X), (Y1 - Y));
        }
    }
}
