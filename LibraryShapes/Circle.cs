﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LibraryShapes
{
    public class Circle : ShapePoint
    {
        protected int Radius;

        Random rnd = new Random();
        //Констуктор копіювання
        public Circle() : base()
        {
            Radius = rnd.Next(101);
        }
        //Конструктор з параметрами
        public Circle(int x, int y, int radius, Color color) : base(x, y, color)
        {
            Radius = radius;
        }
        //Конструктор копіювання
        public Circle(Circle c) : base(c)
        {
            Radius = c.Radius;
        }

        public void SetCircleRadius(int radius)
        {
            Radius = radius;
        }

        public override void Draw(Graphics gr)
        {            
            Pen p = new Pen(Color, 3);
            gr.DrawEllipse(p, X-Radius, Y-Radius, 2*Radius, 2*Radius);
        }
    }
}