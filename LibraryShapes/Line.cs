﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LibraryShapes
{
    public class Line : ShapePoint
    {
        protected int X1;
        protected int Y1;

        Random rnd = new Random();

        //Конструктор по замовчуванню
        public Line()
        {
            X = rnd.Next(651);
            Y = rnd.Next(401);
            Color = Color.FromArgb(rnd.Next(20, 256), rnd.Next(20, 256), rnd.Next(20, 256));
            X1 = rnd.Next(651);
            Y1 = rnd.Next(401);
        }
        //Конструктор з параметрами
        public Line(int x, int y, Color color, int x1, int y1) : base(x, y, color)
        {
            X1 = x1;
            Y1 = y1;
        }
        //Конструктор копіювання
        public Line(Line line) : base(line)
        {
            X1 = line.X1;
            Y1 = line.Y1;
        }

        public void SetLineEnd(int x1, int y1)
        {
            X1 = x1;
            Y1 = y1;
        }

        public override void Draw(Graphics gr)
        {           
            Pen p = new Pen(Color, 2);
            gr.DrawLine(p, X, Y, X1, Y1);
        }
    }
}
