﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace LibraryShapes
{
    public class ShapePoint
    {
        protected int X;
        protected int Y;
        protected Color Color;

        Random rnd = new Random();

        //Конструктор по замовчуванню
        public ShapePoint()
        {
            X = rnd.Next(651);
            Y = rnd.Next(401);
            Color = Color.FromArgb(rnd.Next(20, 256), rnd.Next(20, 256), rnd.Next(20, 256));
        }
        //Конструктор з параметрами
        public ShapePoint(int x, int y, Color color)
        {
            X = x;
            Y = y;
            Color = color;
        }

        //Конструктор копіювання
        public ShapePoint(ShapePoint point)
        {
            X = point.X;
            Y = point.Y;
            Color = point.Color;
        }

        public void SetShapePoint(int x, int y)
        {
            X = x;
            Y = y;
        }

        public virtual void Draw(Graphics gr)
        {
            SolidBrush brush = new SolidBrush(Color);
            Pen p = new Pen(Color, 1);
            gr.DrawEllipse(p, X, Y, 3, 3);
            gr.FillEllipse(brush, X, Y, 3, 3);
            
        }
    }
}
