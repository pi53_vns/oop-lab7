﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLab7
{
    public class Student : Person
    {
        protected uint Course;
        protected string Group;
        protected string Faculty;
        protected string University;

        //Конструктор по замовчуванню
        public Student() : base()
        {
            Course = 1;
            Group = "ПІ-53";
            Faculty = "ФІКТ";
            University = "ЖДТУ";
        }
        //Конструктор з параметрами
        public Student(string name, string surname, uint day, uint month, uint year, uint course, string group, string faculty) : base(day, month, year)
        {
            Course = course;
            Group = group;
            Faculty = faculty;
        }
        //Конструктор з параметрами
        public Student(string name, string surname, uint day, uint month, uint year, string university) : base(name, surname)
        {
            University = university;
        }
        //Конструктор копіювання
        public Student(Student student) : base(student)
        {
            Course = student.Course;
            Group = student.Group;
            Faculty = student.Faculty;
            University = student.University;
        }

        public void SetCourse(uint course)
        {
            if (course >= 1 && course <= 6)
                Course = course;
        }
        public uint GetCourse()
        {
            return Course;
        }

        public void SetGroup(string group)
        {
            if (group.Length > 0)
                Group = group;
        }
        public string GetGroup()
        {
            return Group;
        }

        public void SetFaculty(string faculty)
        {
            if (faculty.Length > 0 && String.Compare(faculty, "0123456789") > 0)
                Faculty = faculty;
        }
        public string GetFaculty()
        {
            return Faculty;
        }

        public void SetUniversity(string university)
        {
            if (university.Length > 0 && String.Compare(university, "0123456789") > 0)
                University = university;
        }
        public string GetUniversity()
        {
            return University;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"Студент\nІм'я: {Name}\nПрізвище: {Surname}");
            if (Month < 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.0{Month}.{Year}");
            else if (Month < 10 && Day >= 10)
                Console.WriteLine($"День народження: {Day}.0{Month}.{Year}");
            else if (Month >= 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.{Month}.{Year}");
            else
                Console.WriteLine($"День народження: {Day}.{Month}.{Year}");
            Console.WriteLine($"Курс: {Course}\nГрупа: {Group}\nФакультет: {Faculty}\nВНЗ: {University}\n");            
        }
    }
}
