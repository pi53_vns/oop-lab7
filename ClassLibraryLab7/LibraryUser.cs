﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLab7
{
    public class LibraryUser : Person
    {
        protected uint LibraryCard;
        protected uint IssueDay;
        protected uint IssueMonth;
        protected uint IssueYear;
        protected uint MonthFee;

        //Конструктор по замовчуванню
        public LibraryUser() : base()
        {
            LibraryCard = 123456;
            IssueDay = 5;
            IssueMonth = 3;
            IssueYear = 2014;
            MonthFee = 20;
        }
        //Конструктор з параметрами
        public LibraryUser(string name, string surname, uint day, uint month, uint year, uint libraryCard) : base(name, surname)
        {
            LibraryCard = libraryCard;
        }
        //Конструктор з параметрами
        public LibraryUser(string name, string surname, uint day, uint month, uint year, uint issueYear, uint monthFee) : base(name, surname)
        {
            IssueYear = issueYear;
            MonthFee = monthFee;
        }
        //Конструктор копіювання
        public LibraryUser(LibraryUser libuser) : base(libuser)
        {
            LibraryCard = libuser.LibraryCard;
            IssueDay = libuser.IssueDay;
            IssueMonth = libuser.IssueMonth;
            IssueYear = libuser.IssueYear;
            MonthFee = libuser.MonthFee;
        }

        public void SetLibraryCard(uint libraryCard)
        {
            if (libraryCard > 0)
                LibraryCard = libraryCard;
        }
        public uint GetLibraryCard()
        {
            return LibraryCard;
        }

        public void SetIssueDay(uint issueDay)
        {
            if (issueDay > 0 && issueDay < 31)
                IssueDay = issueDay;
        }
        public uint GetIssueDay()
        {
            return IssueDay;
        }

        public void SetIssueMonth(uint issueMonth)
        {
            if (issueMonth > 0 && issueMonth < 13)
                IssueMonth = issueMonth;
        }
        public uint GetIssueMonth()
        {
            return IssueMonth;
        }

        public void SetIssueYear(uint issueYear)
        {
            if (issueYear >= 2000 && issueYear < DateTime.Now.Year || (issueYear == DateTime.Now.Year && IssueMonth <= DateTime.Now.Month && IssueDay <= DateTime.Now.Day))
                IssueYear = issueYear;
        }
        public uint GetIssueYear()
        {
            return IssueYear;
        }

        public void SetMonthFee(uint monthFee)
        {
            if (monthFee > 20)
                MonthFee = monthFee;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"Користувач бібліотеки\nІм'я: {Name}\nПрізвище: {Surname}");
            if (Month < 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.0{Month}.{Year}");
            else if (Month < 10 && Day >= 10)
                Console.WriteLine($"День народження: {Day}.0{Month}.{Year}");
            else if (Month >= 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.{Month}.{Year}");
            else
                Console.WriteLine($"День народження: {Day}.{Month}.{Year}");
            Console.WriteLine($"Номер читацького квитка: {LibraryCard}");
            if (Month < 10 && Day < 10)
                Console.WriteLine($"Дата видачі: 0{Day}.0{Month}.{Year}");
            else if (Month < 10 && Day >= 10)
                Console.WriteLine($"Дата видачі: {Day}.0{Month}.{Year}");
            else if (Month >= 10 && Day < 10)
                Console.WriteLine($"Дата видачі: 0{Day}.{Month}.{Year}");
            else
                Console.WriteLine($"Дата видачі: {Day}.{Month}.{Year}");
            Console.WriteLine($"Розмір щомісячного читацького внеску: {MonthFee}\n");
        }

    }
}
