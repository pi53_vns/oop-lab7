﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLab7
{
    public class Entrant : Person
    {
        protected float FirstZNO;
        protected float SecondZNO;
        protected float ThirdZNO;
        protected float FourthZNO;
        protected uint EducationDocument;
        protected string SchoolName;

        //Конструктор по замовчуванню
        public Entrant() : base()
        {
            FirstZNO = 194;
            SecondZNO = 177;
            ThirdZNO = 180;
            FourthZNO = 0;
            EducationDocument = 196;
            SchoolName = "ЖСШ №20";
        }
        //Конструктор з параметрами
        public Entrant(string name, string surname, uint day, uint month, uint year, float firstZNO, float secondZNO, float thirdZNO, float fourthZNO) : base(name, surname)
        {
            FirstZNO = firstZNO;
            SecondZNO = secondZNO;
            ThirdZNO = thirdZNO;
            FourthZNO = fourthZNO;
        }
        //Конструктор з параметрами
        public Entrant(string name, string surname, uint day, uint month, uint year, uint educationDocument, string schoolName) : base(name, surname)
        {
            EducationDocument = educationDocument;
            SchoolName = schoolName;
        }
        //Конструктор копіювання
        public Entrant(Entrant entrant) : base(entrant)
        {
            FirstZNO = entrant.FirstZNO;
            SecondZNO = entrant.SecondZNO;
            ThirdZNO = entrant.ThirdZNO;
            FourthZNO = entrant.FourthZNO;
            EducationDocument = entrant.EducationDocument;
            SchoolName = entrant.SchoolName;
        }

        public void SetFirstZNO(float firstZNO)
        {
            if (firstZNO >= 100 && firstZNO <= 200)
                FirstZNO = firstZNO;
        }
        public float GetFirstZNO()
        {
            return FirstZNO;
        }

        public void SetSecondZNO(float secondZNO)
        {
            if (secondZNO >= 100 && secondZNO <= 200)
                SecondZNO = secondZNO;
        }
        public float GetSecondZNO()
        {
            return SecondZNO;
        }

        public void SetThirdZNO(float thirdZNO)
        {
            if (thirdZNO >= 100 && thirdZNO <= 200)
                ThirdZNO = thirdZNO;
        }
        public float GetThirdZNO()
        {
            return ThirdZNO;
        }

        public void SetFourthZNO(float fourthZNO)
        {
            if (fourthZNO >= 100 && fourthZNO <= 200)
                FourthZNO = fourthZNO;
        }
        public float GetFourthZNO()
        {
            return FourthZNO;
        }

        public void SetEducationDocument(uint educationDocument)
        {
            if (educationDocument >= 100 && educationDocument <= 200)
                EducationDocument = educationDocument;
        }
        public uint GetEducationDocument()
        {
            return EducationDocument;
        }

        public void SetSchoolName(string schoolName)
        {
            if (schoolName.Length > 0)
                SchoolName = schoolName;
        }
        public string GetSchoolName()
        {
            return SchoolName;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"Абітурієнт\nІм'я: {Name}\nПрізвище: {Surname}");
            if (Month < 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.0{Month}.{Year}");
            else if (Month < 10 && Day >= 10)
                Console.WriteLine($"День народження: {Day}.0{Month}.{Year}");
            else if (Month >= 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.{Month}.{Year}");
            else
                Console.WriteLine($"День народження: {Day}.{Month}.{Year}");
            Console.WriteLine($"Кількість балів першого предмету ЗНО: {FirstZNO:F1}");
            Console.WriteLine($"Кількість балів другого предмету ЗНО: {SecondZNO:F1}");
            Console.WriteLine($"Кількість балів третього предмету ЗНО: {ThirdZNO:F1}");
            Console.WriteLine($"Кількість балів четвертого предмету ЗНО: {FourthZNO:F1}");
            Console.WriteLine($"Кількість балів за документ про освіту: {EducationDocument}");
            Console.WriteLine($"Назва загальноосвітнього навчального закладу: {SchoolName}\n");
        }


    }
}
