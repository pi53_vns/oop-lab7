﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLab7
{
    public class Teacher : Person
    {
        protected string Position;
        protected string Department;
        protected string University;

        //Конструктор по замовчуванню
        public Teacher() : base()
        {
            Position = "Декан";
            Department = "Програмного забезпечення систем";
            University = "ЖДТУ";
        }
        //Конструктор з параметрами
        public Teacher(string name, string surname, uint day, uint month, uint year, string position) : base(name, surname)
        {
            Position = position;
        }
        //Конструктор з параметрами
        public Teacher(string name, string surname, uint day, uint month, uint year, string department, string university) : base(name, surname)
        {
            Department = department;
            University = university;
        }
        //Конструктор копіювання
        public Teacher(Teacher teacher) : base(teacher)
        {
            Position = teacher.Position;
            Department = teacher.Department;
            University = teacher.University;
        }

        public void SetPosition(string position)
        {
            if (position.Length > 0 && String.Compare(position, "0123456789") > 0)
                Position = position;
        }
        public string GetPosition()
        {
            return Position;
        }

        public void SetDepartment(string department)
        {
            if (department.Length > 0 && String.Compare(department, "0123456789") > 0)
                Department = department;
        }
        public string GetDepartment()
        {
            return Department;
        }

        public void SetUniversity(string university)
        {
            if (university.Length > 0 && String.Compare(university, "0123456789") > 0)
                University = university;
        }
        public string GetUniversity()
        {
            return University;
        }
        public override void ShowInfo()
        {
            Console.WriteLine($"Викладач\nІм'я: {Name}\nПрізвище: {Surname}");
            if (Month < 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.0{Month}.{Year}");
            else if (Month < 10 && Day >= 10)
                Console.WriteLine($"День народження: {Day}.0{Month}.{Year}");
            else if (Month >= 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.{Month}.{Year}");
            else
                Console.WriteLine($"День народження: {Day}.{Month}.{Year}");
            Console.WriteLine($"Посада: {Position}\nКафедра: {Department}\nВНЗ: {University}\n");          
        }
    }
}