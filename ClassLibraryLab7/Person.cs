﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLab7
{
    public class Person
    {
        protected string Name;
        protected string Surname;
        protected uint Day;
        protected uint Month;
        protected uint Year;

        //Конструктор по замовчуванню
        public Person()
        {
            Name = "Наталія";
            Surname = "Волинець";
            Day = 26;
            Month = 1;
            Year = 1999;
        }
        //Конструктор з параметрами
        public Person(string name, string surname)
        {
            Name = name;
            Surname = surname;
            Day = 14;
            Month = 7;
            Year = 1987;
        }
        //Конструктор з параметрами
        public Person(uint day, uint month, uint year)
        {
            Name = "Петро";
            Surname = "Петров";
            Day = day;
            Month = month;
            Year = year;
        }
        //Конструктор копіювання
        public Person(Person person)
        {
            Name = person.Name;
            Surname = person.Surname;
            Day = person.Day;
            Month = person.Month;
            Year = person.Year;
        }
        
        
        public void SetName(string name)
        {
            if (name.Length > 0 && String.Compare(name, "0123456789") > 0)
                Name = name;
        }
        public string GetName()
        {
            return Name;
        }
        public void SetSurname(string surname)
        {
            if (surname.Length > 0 && String.Compare(surname, "0123456789") > 0)
                Surname = surname;
        }
        public string GetSurname()
        {
            return Surname;
        }
        public void SetDay(uint day)
        {
            if (day > 0 && day < 31)
                Day = day;
        }
        public uint GetDay()
        {
            return Day;
        }
        public void SetMonth(uint month)
        {
            if (month > 0 && month < 13)
                Month = month;
        }
        public uint GetMonth()
        {
            return Month;
        }
        public void SetYear(uint year)
        {
            if (year >= 1900 && year < DateTime.Now.Year || (year == DateTime.Now.Year && Month <= DateTime.Now.Month && Day <= DateTime.Now.Day))
                Year = year;
        }
        public uint GetYear()
        {
            return Year;
        }
        public virtual void ShowInfo()
        {
            Console.WriteLine($"Людина\nІм'я: {Name}\nПрізвище: {Surname}");
            if (Month < 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.0{Month}.{Year}\n");
            else if (Month < 10 && Day >= 10)
                Console.WriteLine($"День народження: {Day}.0{Month}.{Year}\n");
            else if (Month >= 10 && Day < 10)
                Console.WriteLine($"День народження: 0{Day}.{Month}.{Year}\n");
            else
                Console.WriteLine($"День народження: {Day}.{Month}.{Year}\n");
        }
    }
}
